package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.dto.GoodsSalesDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.entity.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Mapper
public interface OrderMapper {
    /**
     * 插入订单数据
     * @param orders
     */
    void insert(Orders orders);

    /**
     * 根据订单号查询订单
     * @param orderNumber
     */
    @Select("select * from orders where number = #{orderNumber}")
    Orders getByNumber(String orderNumber);

    /**
     * 修改订单信息
     * @param orders
     */
    void update(Orders orders);
    /**
     * 根据id查询订单
     *
     * @param id
     * @return
     */
    @Select("select *from orders where id=#{id}")
    Orders getById(Long id);
    /**
     * 历史订单查询
     */
    Page<Orders> pageQuery1(OrdersPageQueryDTO ordersPageQueryDTO);

    /**
     * 根据订单状态查询数量
     * @param statsu
     * @return
     */
    @Select("select count(*)from orders where status=#{statsu}")
    Integer countStatus(Integer statsu);

    /**
     * 根据订单状态和下单时间查询订单
     * @param status
     * @param orderTime
     * @return
     */
    @Select("select * from orders where status = #{status} and order_time < #{orderTime}")
    List<Orders> getByStatusAndOrderTime(Integer status, LocalDateTime orderTime);
    /**
     * 营业额统计
     * @return
     */
   // @Select("select sum(amount) from orders where  #{begin}< order_time <#{end} and status=#{status}")
    Double sumByMap(Map map);
    /**
     * 根据条件统计订单数量
     * @return
     */
    Integer countByMap(Map map);
    /**
     * 统计指定时间区间内的销量排名前10
     */
    List<GoodsSalesDTO> getSalesTop10(LocalDateTime begin, LocalDateTime end);
}
