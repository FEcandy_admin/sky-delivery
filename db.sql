CREATE TABLE `ht_pay`.`category`
(
    `id` int NOT NULL AUTO_INCREMENT,
    `type` varchar(255) NULL DEFAULT NULL,
    `name`        varchar(255) NULL DEFAULT NULL,
    `sort`       varchar(255) NULL DEFAULT NULL,
    `status`      varchar(255) NULL DEFAULT NULL,
    `create_time` varchar(255) NULL DEFAULT NULL,
    `update_time` varchar(255) NULL DEFAULT NULL,
    `create_user` varchar(255) NULL DEFAULT NULL,
    `update_user` varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = Dynamic;



CREATE TABLE `ht_pay`.`address_book`
(
    `id` int NOT NULL AUTO_INCREMENT,
    `user_id` varchar(255) NULL DEFAULT NULL,
    `consignee`        varchar(255) NULL DEFAULT NULL,
    `phone`        varchar(255) NULL DEFAULT NULL,
    `sex`        varchar(255) NULL DEFAULT NULL,
    `province_code`        varchar(255) NULL DEFAULT NULL,
    `province_name`        varchar(255) NULL DEFAULT NULL,
    `city_code`        varchar(255) NULL DEFAULT NULL,
    `city_name`        varchar(255) NULL DEFAULT NULL,
    `district_code`        varchar(255) NULL DEFAULT NULL,
    `district_name`        varchar(255) NULL DEFAULT NULL,
    `detail`        varchar(255) NULL DEFAULT NULL,
    `label`        varchar(255) NULL DEFAULT NULL,
    `is_default`        varchar(255) NULL DEFAULT NULL,
    `status`      varchar(255) NULL DEFAULT NULL,
    `create_time` varchar(255) NULL DEFAULT NULL,
    `update_time` varchar(255) NULL DEFAULT NULL,
    `create_user` varchar(255) NULL DEFAULT NULL,
    `update_user` varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = Dynamic;

CREATE TABLE `ht_pay`.`dish`
(
    `id` int NOT NULL AUTO_INCREMENT,
    `category_id` varchar(255) NULL DEFAULT NULL,
    `name`        varchar(255) NULL DEFAULT NULL,
    `price`       varchar(255) NULL DEFAULT NULL,
    `status`      varchar(255) NULL DEFAULT NULL,
    `create_time` varchar(255) NULL DEFAULT NULL,
    `update_time` varchar(255) NULL DEFAULT NULL,
    `create_user` varchar(255) NULL DEFAULT NULL,
    `update_user` varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = Dynamic;



CREATE TABLE `ht_pay`.`dish_flavor`
(
    `id` int NOT NULL AUTO_INCREMENT,
    `dish_id`     varchar(255) NULL DEFAULT NULL,
    `name`        varchar(255) NULL DEFAULT NULL,
    `create_time` varchar(255) NULL DEFAULT NULL,
    `update_time` varchar(255) NULL DEFAULT NULL,
    `create_user` varchar(255) NULL DEFAULT NULL,
    `update_user` varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = Dynamic;



CREATE TABLE `ht_pay`.`employee`
(
    `id` int NOT NULL AUTO_INCREMENT,
    `username`    varchar(255) NULL DEFAULT NULL,
    `name`        varchar(255) NULL DEFAULT NULL,
    `password`    varchar(255) NULL DEFAULT NULL,
    `phone`       varchar(255) NULL DEFAULT NULL,
    `sex`         varchar(255) NULL DEFAULT NULL,
    `id_number`   varchar(255) NULL DEFAULT NULL,
    `status`      varchar(255) NULL DEFAULT NULL,
    `create_time` varchar(255) NULL DEFAULT NULL,
    `update_time` varchar(255) NULL DEFAULT NULL,
    `create_user` varchar(255) NULL DEFAULT NULL,
    `update_user` varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = Dynamic;



CREATE TABLE `ht_pay`.`order_detail`
(
    `id` int NOT NULL AUTO_INCREMENT,
    `order_id`    varchar(255) NULL DEFAULT NULL,
    `name`        varchar(255) NULL DEFAULT NULL,
    `dish_id`     varchar(255) NULL DEFAULT NULL,
    `setmeal_id`  varchar(255) NULL DEFAULT NULL,
    `dish_flavor` varchar(255) NULL DEFAULT NULL,
    `number`      varchar(255) NULL DEFAULT NULL,
    `amount`      varchar(255) NULL DEFAULT NULL,
    `image`       varchar(255) NULL DEFAULT NULL,
    `status`      varchar(255) NULL DEFAULT NULL,
    `create_time` varchar(255) NULL DEFAULT NULL,
    `update_time` varchar(255) NULL DEFAULT NULL,
    `create_user` varchar(255) NULL DEFAULT NULL,
    `update_user` varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = Dynamic;



CREATE TABLE `ht_pay`.`orders`
(
    `id`                      varchar(255) NOT NULL,
    `number`                  varchar(255) NULL DEFAULT NULL,
    `status`                  varchar(255) NULL DEFAULT NULL,
    `user_id`                 varchar(255) NULL DEFAULT NULL,
    `address_book_id`         varchar(255) NULL DEFAULT NULL,
    `order_time`              varchar(255) NULL DEFAULT NULL,
    `checkout_time`           varchar(255) NULL DEFAULT NULL,
    `pay_method`              varchar(255) NULL DEFAULT NULL,
    `pay_status`              varchar(255) NULL DEFAULT NULL,
    `amount`                  varchar(255) NULL DEFAULT NULL,
    `remark`                  varchar(255) NULL DEFAULT NULL,
    `user_name`               varchar(255) NULL DEFAULT NULL,
    `phone`                   varchar(255) NULL DEFAULT NULL,
    `address`                 varchar(255) NULL DEFAULT NULL,
    `consignee`               varchar(255) NULL DEFAULT NULL,
    `cancel_reason`           varchar(255) NULL DEFAULT NULL,
    `rejection_reason`        varchar(255) NULL DEFAULT NULL,
    `cancel_time`             varchar(255) NULL DEFAULT NULL,
    `estimated_delivery_time` varchar(255) NULL DEFAULT NULL,
    `delivery_status`         varchar(255) NULL DEFAULT NULL,
    `delivery_time`           varchar(255) NULL DEFAULT NULL,
    `pack_amount`             varchar(255) NULL DEFAULT NULL,
    `tableware_number`        varchar(255) NULL DEFAULT NULL,
    `tableware_status`        varchar(255) NULL DEFAULT NULL,
    `create_time`             varchar(255) NULL DEFAULT NULL,
    `update_time`             varchar(255) NULL DEFAULT NULL,
    `create_user`             varchar(255) NULL DEFAULT NULL,
    `update_user`             varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = Dynamic;



CREATE TABLE `ht_pay`.`setmeal`
(
    `id` int NOT NULL AUTO_INCREMENT,
    `category_id` varchar(255) NULL DEFAULT NULL,
    `name`        varchar(255) NULL DEFAULT NULL,
    `price`       varchar(255) NULL DEFAULT NULL,
    `description` varchar(255) NULL DEFAULT NULL,
    `image`       varchar(255) NULL DEFAULT NULL,
    `status`      varchar(255) NULL DEFAULT NULL,
    `create_time` varchar(255) NULL DEFAULT NULL,
    `update_time` varchar(255) NULL DEFAULT NULL,
    `create_user` varchar(255) NULL DEFAULT NULL,
    `update_user` varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = Dynamic;



CREATE TABLE `ht_pay`.`setmeal_dish`
(
    `id` int NOT NULL AUTO_INCREMENT,
    `setmeal_id`  varchar(255) NULL DEFAULT NULL,
    `dish_id`     varchar(255) NULL DEFAULT NULL,
    `name`        varchar(255) NULL DEFAULT NULL,
    `price`       varchar(255) NULL DEFAULT NULL,
    `copies`      varchar(255) NULL DEFAULT NULL,
    `status`      varchar(255) NULL DEFAULT NULL,
    `create_time` varchar(255) NULL DEFAULT NULL,
    `update_time` varchar(255) NULL DEFAULT NULL,
    `create_user` varchar(255) NULL DEFAULT NULL,
    `update_user` varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = Dynamic;



CREATE TABLE `ht_pay`.`shopping_cart`
(
    `id` int NOT NULL AUTO_INCREMENT,
    `name`        varchar(255) NULL DEFAULT NULL,
    `user_id`     varchar(255) NULL DEFAULT NULL,
    `dish_id`     varchar(255) NULL DEFAULT NULL,
    `setmeal_id`  varchar(255) NULL DEFAULT NULL,
    `dish_flavor` varchar(255) NULL DEFAULT NULL,
    `number`      varchar(255) NULL DEFAULT NULL,
    `amount`      varchar(255) NULL DEFAULT NULL,
    `image`       varchar(255) NULL DEFAULT NULL,
    `status`      varchar(255) NULL DEFAULT NULL,
    `create_time` varchar(255) NULL DEFAULT NULL,
    `update_time` varchar(255) NULL DEFAULT NULL,
    `create_user` varchar(255) NULL DEFAULT NULL,
    `update_user` varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = Dynamic;


CREATE TABLE `ht_pay`.`user`
(
    `id` int NOT NULL AUTO_INCREMENT,
    `openid`      varchar(255) NULL DEFAULT NULL,
    `name`        varchar(255) NULL DEFAULT NULL,
    `phone`       varchar(255) NULL DEFAULT NULL,
    `sex`         varchar(255) NULL DEFAULT NULL,
    `id_number`   varchar(255) NULL DEFAULT NULL,
    `avatar`      varchar(255) NULL DEFAULT NULL,
    `status`      varchar(255) NULL DEFAULT NULL,
    `create_time` varchar(255) NULL DEFAULT NULL,
    `update_time` varchar(255) NULL DEFAULT NULL,
    `create_user` varchar(255) NULL DEFAULT NULL,
    `update_user` varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ROW_FORMAT = Dynamic;





